

public class EmployeeUtil {
    public static final double EURO = 5;

    public boolean isRetire(int age) {
        if (age < 65) {
            return false;
        }
        return true;
    }

    public double convertSalaryToEur(Employee employee) {
        return employee.getSalary() / EURO;
    }

}
