import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

public class EmployeeUtilTest {
    @ParameterizedTest
    @CsvSource({"1000,200", "2000,400"})
    public void convertSalaryToEurShouldBeSuccesfull(double inputSalary, double expectedEuroSalary) {

        //Given
        EmployeeUtil employeeUtil = new EmployeeUtil();
        Employee employee1 = new Employee();
        employee1.setSalary(inputSalary);
        //When
        double actualEuroSalary = employeeUtil.convertSalaryToEur(employee1);
        //Then
        Assertions.assertEquals(expectedEuroSalary, actualEuroSalary);
    }

    @ParameterizedTest
    @ValueSource(ints = {66, 71, 80, 75})
    public void isRetiredShouldReturnTrue(int age) { //numele motodei + Should + rezultatul asteptat

        //Given
        EmployeeUtil employeeUtil = new EmployeeUtil();
        boolean actualIsRetiredResult;
        //When
        actualIsRetiredResult = employeeUtil.isRetire(age);

        //Then
        Assertions.assertEquals(true,actualIsRetiredResult);
    }

    @ParameterizedTest
    @CsvSource({"50,false", "65,true", "70,true","25,false"})
    public void isRetiredShouldBeSuccesfull(int inputAge, boolean expectedIsRetiredResult){

        //Given
        EmployeeUtil employeeUtil = new EmployeeUtil();
        //When
        boolean actualIsRetiredResult = employeeUtil.isRetire(inputAge);
        //Then
        Assertions.assertEquals(expectedIsRetiredResult,actualIsRetiredResult);
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForIsRetired")
    public void isRetiredShouldBeSuccesfull2(int inputAge, boolean expectedIsRetiredResult){

        //Given
        EmployeeUtil employeeUtil = new EmployeeUtil();
        //When
        boolean actualIsRetiredResult = employeeUtil.isRetire(inputAge);
        //Then
        Assertions.assertEquals(expectedIsRetiredResult,actualIsRetiredResult);
    }

    public static Stream<Arguments> provideArgumentsForIsRetired(){
        return Stream.of(
                Arguments.of(50,false),
                Arguments.of(65,true),
                Arguments.of(70,true));
    }

}
