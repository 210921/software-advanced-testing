import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTest {

    @Mock
    EmployeeRepository employeeRepository;
    @InjectMocks
    EmployeeService employeeService;

    @BeforeEach
    public void setUp() {
        //employeeService=new EmployeeService(employeeRepository);
    }

    @Test
    public void findByNameShouldReturnCorectEmployee() {
        //given
        when(employeeRepository.findByName("Gheorghe")).thenReturn(new Employee("Gheorghe", "top@email", 26, 4000));


        //when
        Employee acutualEmployee = employeeService.findByName("Gheorghe");


        //then
        Assertions.assertEquals("Gheorghe", acutualEmployee.getName());
        Assertions.assertEquals("top@email", acutualEmployee.getEmail());

    }

}
