import org.assertj.core.api.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class EmployeeTest {

    @Test
    public void setSalaryShouldThrowException() {  //structura: numele metodei + should + exceptia
        // given
        Employee employee = new Employee();
        //when
        try {
            employee.setSalary(-1000);
            fail("Exception not thrown");

        } catch (IllegalArgumentException i) {
            assertThat(i.getMessage().equals("Salary cannot be negative"));
        }
    }

    @ParameterizedTest
    @ValueSource(doubles = {-5, -10, -30})
    public void setSalaryShouldThrowException2(double salary) {
        // given
        Employee employee = new Employee();
        // when
        IllegalArgumentException illegal = Assertions.assertThrows(IllegalArgumentException.class,
                () -> {
                    employee.setSalary(salary);
                });
        // than
//        assertThat(illegal.getMessage().equals("Salary cannot be negative"));
        assertEquals("Salary cannot be negative", illegal.getMessage());

    }


}
